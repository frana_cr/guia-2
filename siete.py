#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se establecen las listas con variables
lista1 = ["Hola", "Hola", "Hola", "Gracias", "Buen dia"]
lista2 = ["Chao", "Adios", "Gracias", "Hola"]
lista3 = []
lista_comun = []
listasolo1 = []
listasolo2 = []


# La funcion mostrara la lista con todos los valores
def lista_total(lista1, lista2, lista3):
    lista3 = lista1 + lista2
    print("Lista total", lista3)


# La funcion mostrara las palabras comunes(ambas listas)
def palabras_comun(lista1, lista2):
    global lista_comun
    for i in lista1:
        if i in lista2:
            lista_comun += [i]
    print("Palabras comun:", lista_comun)


# Se mostrara solo las palabras de la lista 2
def palabras_solo1(lista1, lista2):
    global listasolo1
    for i in lista1:
        if i not in lista2:
            listasolo1 += [i]
    print("Palabras solo 1:", listasolo1)


# Se entregaran las palabras solo en la lista 2
def palabras_solo2(lista1, lista2):
    global listasolo2
    for i in lista2:
        if i not in lista1:
            listasolo2 += [i]
    print("Palabras solo 2:", listasolo2)


# Se llama a las funciones
lista_total(lista1, lista2, lista3)
palabras_comun(lista1, lista2,)
palabras_solo1(lista1, lista2)
palabras_solo2(lista2, lista2)
