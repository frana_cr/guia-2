#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se ingresan las variables y las listas
lista1 = ["Hola", "Hola", "Hola", "gracias", "gracias", "Chao"]
lista2 = ["Hola", "Chao", "Como Estas"]
lista_aleatoria = lista1 + lista2
lista_original = lista_aleatoria
lista_nueva = []
cuenta1 = 0
cuenta2 = 0
cuenta3 = 0
cuenta4 = 0


# Se crea la funcion eliminar repetido
def eliminar(lista_original):
    for i in lista_original:
        if i not in lista_nueva:
            lista_nueva.append(i)
    print("La lista sin repetir", lista_nueva)


# Se cuenta a los elementos de la lista
def contarElementosLista(lista_aleatoria):
    cuenta1 = lista_aleatoria.count("Como estas")
    cuenta2 = lista_aleatoria.count("gracias")
    cuenta3 = lista_aleatoria.count("Chao")
    cuenta4 = lista_aleatoria.count("Como Estas")


# Se ve el elemento mas repetido segun el contador
# El valor tendra que ser mayor o igual a 3
def mayor_repeticion(cuenta1, cuenta2, cuenta3, cuenta4):
    if(cuenta1 >= 3):
        cuenta1 = cuenta1 + 1
        print("Es el mas repetido Como Estas")
    elif(cuenta2 >= 3):
        cuenta2 = cuenta2 + 1
        print("Es el mas repetido gracias")
    elif(cuenta3 >= 3):
        cuenta3 = cuenta3 + 1
        print("Es el mas repetido Chao")
    else:
        print("Es el mas repetido Hola")


# Se ve el elemento menos repetido  segun el valor especifico
# El valor del contador tiene que ser menor a 1
def menor_repeticion(cuenta1, cuenta2, cuenta3, cuenta4):
    if(cuenta1 <= 1):
        cuenta1 = cuenta1 + 1
        print("Es el menos repetido Como Estas")
    elif(cuenta2 <= 1):
        cuenta2 = cuenta2 + 1
        print("Es el menos repetido gracias")
    elif(cuenta3 <= 1):
        cuenta3 = cuenta3 + 1
        print("Es el menos repetido Chao")
    else:
        print("Es el menos repetido Hola")


# Se imprime la lista y llama funciones
print("Lista original", lista_aleatoria)
eliminar(lista_original)
contarElementosLista(lista_aleatoria)
mayor_repeticion(cuenta1, cuenta2, cuenta3, cuenta4)
menor_repeticion(cuenta1, cuenta2, cuenta3, cuenta4)
