#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se pide al usuario que añada la altura
altura = int(input(" Ingrese la altura del triangulo: "))


# Se define la funcion triangular izquierda
def trian_izquierda(altura):
	print("\n")
	# Se recorre el for para formar un triangulo de modo izquierdo
	for i in range(altura + 1):
		espacios = altura - 0
		print("*" *i)


# Se define la funcion triangular izquierda
def trian_derecha(altura):
	print("\n")
	# Se recorre el for para formar un triangulo de modo derecho
	for i in range(altura + 1):
	    # La altura se disminuira por la posicion
		espacios = altura-i
	print(" "*espacios + "*"*i)


# Se define la funcion lateral izquierda
def lateral_izq(altura):
	print("\n")
	# Por medio del for se formara como la mitad del rombo
	for i in range(altura):
		for j in range(i):
			print("*", end="")
		print("*")
		# Recorrera por dos formas el triangulo
	for i in range(altura, 0, -1):
		for j in range(i):
			print("*", end="")
		print("*")


# Se define la funcion lateral izquierda en sentido contrario
def lateral_der(altura):
	print("\n")
	# Como el sentido sera inverso la se cambiaran las ubicaciones de los for
	for i in range(altura, 0, -1):
		for j in range(i):
			print("*", end="")
		print("*")
		# Se cambiara el orden de ubicacion
	for i in range(altura):
		for j in range(i):
			print("*", end="")
		print("*")


# Se llaman a la funciones creadas
trian_derecha(altura)
trian_izquierda(altura)
lateral_der(altura)
lateral_izq(altura)

# Referenciado de: https://www.youtube.com/watch?v=YhGP6w-XwYQ