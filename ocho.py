#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se importa el random y se pide tamano al usuario
import random
matrix = []
tamano = int(input("Ingrese el tamaño de la matrix"))


# Se creara la matriz por medio de la funcion
def creacion_matrix(matrix, tamano):
    print("\n")
    print("La matrix original es:")
    # Se recorreran filas y columnas de tamano
    for i in range(tamano):
        print("\n")
        matrix.append([])
        for j in range(tamano):
            matrix[i].append([])
            # La matriz tendra valores aleatorios
            matrix[i][j] = random.randint(0, 37)
        print(matrix[i], end="")


# Se crea la funcion que invierte la matriz
def inversa(matrix, tamano):
    print("\n")
    print("La matrix inversa es:")
    # Se recorrera en sentido inverso las filas y columnas
    for i in range(tamano - 1, - 1, -1):
        print("\n")
        for j in range(tamano):
            print([matrix[i][j]], end="")


# Se llama a las funciones
creacion_matrix(matrix, tamano)
inversa(matrix, tamano)
