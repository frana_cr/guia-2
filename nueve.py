#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se pide el tamano al usuario
tamano = int(input("Ingrese tamaño matrix"))


# Se crea la funcion matrix donde se desarrolla la variable identidad
def creacion_matrix(tamano):
    # Se recorrera la matriz en un intervalo especifico
    matrix = [[0] * tamano for i in range(tamano)]
    for i in range(tamano):
        for j in range(tamano):
            # Se evaluaran tres caso de los valores de i y j
            if(i < j):
                matrix[i][j] = 0
            elif(i == j):
                matrix[i][j] = 1
            elif (i > j):
                matrix[i][j] = 0
            # Por medio del for se diseñara la matriz
    for row in matrix:
        print(' '. join([str(elem) for elem in row]))


# Se llama la funcion
creacion_matrix(tamano)
