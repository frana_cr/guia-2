#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se pide al usuario que ingrese la altura
altura = int(input("Ingrese la altura de los triangulos: "))


# Se define la funcion que restringue las alturas
def restriccion_altura(altura):
	if (altura <= 1):
		print("Ingrese nuevamente una altura correcta")
		altura = int(input("Ingrese la altura de los triangulos: "))
	else:
		print("Altura correcta")


# Se define la funcion que formara el triangulo izquierdo
def trian_izquierda(altura):
	for i in range(altura + 1):
		espacios = altura - 0
		print("*" *i)


# Se define la funcion que formara el triangulo derecho
def trian_derecha(altura):
	for i in range(altura + 1):
		espacios = altura - i
		print(" "*espacios + "*"*i)


# Se llaman las funciones
restriccion_altura(altura)
trian_derecha(altura)
trian_izquierda(altura)

# Referenciado de: https://www.youtube.com/watch?v=YhGP6w-XwYQ
