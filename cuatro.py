#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se crea una lista de numeros, y variables
lista_numeros = [3, 6, 5, 4, 15, 20, 5, 2, 3, 8, 9, 55, 60]
lista = []
print("La lista original es:", lista_numeros)
k = 5
a = 0


# Se genera la funcion creciente de numeros
def orden_creciente(lista_numeros):
    lista_numeros.sort(reverse=False)
    print("Lista ordenada creciente:", lista_numeros)


# Se genera la funcion decreciente de numero
def orden_decreciente(lista_numeros):
    lista_numeros.sort(reverse=True)
    print("Lista ordenada decreciente:", lista_numeros)


# Se genera una lista con valores igual a la constante
def iguales_constante(lista_numeros, k):
    a = filter(lambda x: x == k, lista_numeros)
    lista.extend(a)
    print("Valores lista iguales a constante:", lista)


# Se genera una lista con valores multiplos
def multiplos_constante(lista_numeros, k):
    a = filter(lambda x: x % k == 0, lista_numeros)
    lista.extend(a)
    print("Valores de lista multiplos:", lista)


# Se llama a las funciones
orden_creciente(lista_numeros)
orden_decreciente(lista_numeros)
iguales_constante(lista_numeros, k)
multiplos_constante(lista_numeros, k)
