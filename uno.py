#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa el random
import random
# Se crean las listas y definen variables
lista = ['Hola',  'Hola', 'Hola', 'estes', 'bien']
lista2 = ['bienvenido', 'a', 'la', 'tienda', 'gracias', 'por', 'venir']
lista_nueva = []
lista_aleatoria = []
estado = 0


# Se genera la funcion aleatorio con parametros
def generar_aleatorio(estado, lista, lista_aleatoria, lista2):
    estado = random.randint(0, 1)
# Cuando el valor es 0 la lista se añadira a aleatoria
    if(estado == 0):
        lista_aleatoria.append(lista)
# La lista tendra una reversa
        lista.reverse()
# De modo contrario se generar la reversa de la lista2
    else:
        lista_aleatoria.append(lista2)
        lista2.reverse()


generar_aleatorio(estado, lista, lista_aleatoria, lista2)
print("El nuevo orden es:", lista_aleatoria)
