#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
# Se ingresa las variables y el valor aleatorio
lista1 = [42, 11, 5, 17, 9, 10, 31, 51, 2]
lista2 = [3, 5, 27, 18, 10, 45, 60, 1]
lista3 = [1, 5, 7, 24, 17, 72]
lista_aleatoria = lista1 + lista2 + lista3
lista_nueva = []


# La funcion eliminar borrara elementos repetidos
def eliminar(lista_original):
    for i in lista_original:
        if i not in lista_nueva:
            lista_nueva.append(i)
    return lista_nueva


# La funcion reemplazo cambiara pares
def reemplazo(lista_nueva):
    for n, i in enumerate(lista_nueva):
        # Los valores multiplos de 10 e iguales a 0 no cambian
        if ((i % 2 == 0) and ((i % 10 != 0) or (i != 0))):
            # Los valores aleatorios seran entre 1 a 99
            lista_nueva[n] = random.randint(1, 99)
    print("Lista con valores aleatorios", lista_nueva)


# Se llaman las funciones
print("Lista original:", lista_aleatoria)
eliminar(lista_aleatoria)
print("LIsta con valores sin repetir:", lista_nueva)
reemplazo(lista_nueva)
